# Generated by Django 2.2.3 on 2019-08-05 11:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projeto', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='imagem',
            field=models.ImageField(default='', upload_to='produto'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='quantidade',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
    ]
