from django.db import models
from django.conf import settings

class Product(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField()
	imagem = models.ImageField(upload_to='produto')
	quantidade = models.PositiveIntegerField()

	def __str__(self):
		return self.name
