from django.shortcuts import render
from django.views.generic import ListView, DetailView, UpdateView
from projeto.models import Product
from django.shortcuts import redirect

class ProductListView(ListView):
    model = Product

class OutOfProjetoListView(ListView):
    model = Product
    def get_queryset(self):
        products = super().get_queryset()
        return products.filter(quantidade=0)

class AddStockView(DetailView):
    model = Product

    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantidade = self.request.POST['quantidade']
        product.quantidade += int(quantidade)
        product.save()
        return redirect('product_list')

class RemoveStockView(UpdateView):
    model = Product

    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantidade = self.request.POST['quantidade']
        product.quantidade -= int(quantidade)
        product.save()
        return redirect('product_list')

